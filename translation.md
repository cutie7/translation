# 0-9
#### Very Good

---

Sport rules for the army

from the 1st October 1938



I approve the following sport rules for the army. It comes to effect on the 1st October 1938. The inspector of the army school is allowed to arrange corrections in a non fundamental way.


Contents

I. Guiding principles - 9

II. Management, Teachers and art of teaching - 10

III. Education course - 15

IV. The activities - 22
    A. “Körperschule“ (school to shape the body – no english word available) - 22
    
    B. Athletics - 78
    C. Swimming - 92
    D. Boxing - 106
    E. Teamsports - 118
    
V. Appendix - 123


I. Guiding principles

1. These sportive activities raise and preserve the performance of the soldiers body, steels it, awards him strength and endurance, speed and agility. This is why sport is the foundation of the education course for fighting and an inseparable component for the military service.
Sport wakes the offensive spirit, hardens the will, fosters self discipline and with it supports the education of the soldier to a fighter who is ready for action.
Sport forces to subordinate, hardens the bond and spirit of the unit. Next to this it shall give the soldier  relaxation and recreation and sparks joy.

---

# MISSING PAGES 10-14

---


# 15-21
#### Very Good

---

**- 15 -**

III. Training course 
12. The sport must be implemented in the training course in a way that supports the future training with weapons. The year is split into two training parts: 

I. part: Until the personal inspection [literal: single inspection] 

II. part: Rest of the training year 

13.First year of service 

I. part (recruit training): 

The training areas are the physical teaching [literal: body school] and the trail running, which must take place two times each month with increasing difficulty, further boxing. (First training stage.) Swimming opportunities should be primarily used for the training of regular soldiers and nonswimmers. 

II. part: 

a) At the end of April, the trail running training will find its end with standard tournaments. The focus of the training should be boxing, which must be supported until the free fighting. (2. training stage) Physical teaching and team sports may be practiced separately. It should be started with jump, throw and run [or walking] training. Physical teaching, team sports and swimming may be practiced separately. 
 
**- 16 -**

b) In the summer months athletics (except trail running), swimming and team sports should be focused. The physical teaching should be repeated occasionally, gymnastics should be focused with utensils. 

14. 2. year of service 

I. part:

As focus of the winter training is the boxing und should be continued until the free fighting with 3 rounds which last 2 minutes. The physical teaching must be repeated and improved; games, especially team sports are still practiced supporting the exercise material. The trail running must be continued like in the first service year. 

II. part:  

The abilities gained in the first service year regarding athletics and swimming must be improved. The technic of the training must be improved. The Reichssport-Medall and the certificate of the GermanLive-Saving-Cooperation [still exists today as DLRG] must be achieved. Tournaments should bring the physical and mental capabilities of the man to its full potential. The handball and football teams must take part in tournaments in their respective battalions and regiments. (i. to [note quite sure?] 70.) 
  
15.Examine of the training 
 
a) The personal inspection of the recruits stretches itself on all branches of the physical teaching and the boxing school (first training stage) Determined strength and skill practices (for example weight throwing, climbing and grabbing along a rope) [bracket missing?], tournament like obstacle relay race and similar things, mostly paint a better picture of the capabilities of the recruits than a test of the gymnastic training. 
 
**- 17 -**
 
In the second service year especially, boxing must be regarded. The free fighting of 3 round lasting each 3 minutes must be improved. 
Furthermore, there should be improvements in the physical teaching. 
The sergeants should be trained in all winter disciplines. 

b) Until the end of April standard tournaments have to take place. 

e) Once each summer the overall performances in athletics and swimming must be checked. 

Here tournaments between companies of an battalion or regiment can be helpful. The performances below serve as a guideline: 
 
    
Year of service     1.   2. 
100-meter Sprint         14.2 sec.       13.4 sec. 
200-meter Sprint        32.0 sec.       30.0 sec. 
400-meter Sprint        72.0 sec.       68.0 sec. 
broad jump             4,15 m          4.75 m 
high jump             1.15 m          1.35 m 
shot put                6.50 m          8.00 m 
hand grenade throwing      35.00 m         42.00 m 
swimming             100 m in        300 m in  
(free swimming)            2 min & 40 sec. 9 min 
 
16.Tournament and training 

1. In a tournament the soldier learns to give his maximum physical as well as mental capabilities. The tournament is sport requirement for the army.   
 
**- 18 -**
 
thus, an exquisite school of mind, toughness and self-discipline. The achieved top performances in the tournament of particularly qualified soldiers will give the community a motivation for a determined task of self-improvement.  
The young officers and all sergeants take part in official tournaments. It’s desired that every young officer also takes part in unofficial tournament sports. Occasional there may be tournaments in different age groups and years of service, to allow less capable soldiers or beginners to gain success in tournaments. 
Tournaments in the companies and regiment are to happen every year. Division and corps championships will be ordered by the responsible command center. 
Championships of the army or Wehrmacht will be ordered by the high command of the army or Wehrmacht. A command center may be tasked with the execution [of the championship]. 

On each championship with continues performance there must be medical support available. 

At sport presentation only exercises should be shown, that have body improving value.  

2. Sport tournaments are held as single and team competitions. The tournament guidelines of the Deutschen Reichsbundes für Leibesübungen [German Reich-Union for body practice] are crucial for the sport tournaments. 
	
	a) Single tournaments result in top performance at single sport fields [?]. Especially valuable [half of the word is on the next page] 
 
**- 19 –**
 
[repeating last word] valuable here are the all-round exercises, which consist of different exercises for example running, jumping, throwing and swimming.  

b) team tournaments promote the fellowship and esprit de corps. They should play a big role in sports festivals. Suitable here are company relay races, competitions, tug of war etc. To organize a large relay race or the participation in it is of great advertising value. 

3. Weapon tournaments don’t belong in the area of physical education but in the military training. They require a body that is trained thoroughly. The valuation [of the body] is carried out while taking account of sport aspects. 

4. Training 
Soldiers which take part in the tournaments, must be prepared well, because else there would be a risk of injuries. Every soldier which registers for an single competitions, is responsible for is own preparations. He has to be supervised and guided by an experienced officer or an sport sergeant and doctor while training. 

The preparations for the tournament only promise success, if they build upon a good trained body, full health and an exemplarily way of life. 
The main rules of this way of life are: Regular sleep of at least 8 hours, pre-prepared food and avoiding luxury food. Excessive behavior damages the condition and strength. Sexual abstinence is not health damaging [part of the word on the next page] 

**- 20 –**
 
damaging. Especially valuable is sports massage. 
To avoid overexertion of the participants during the preparations an occasional dispensation of other duties must be accepted.
 
17. Off-duty sport, top athletes, army sports association 
18. 
Voluntary off-duty sports activities are supposed to be encouraged through personal example at all ranks, because through that the urge to activity, which is in every youth, will be directed in the right direction. The soldier may practice any sport which he desires in his free time. 

The off-duty sport is regarded as service in the sense of service impermeant [?] if it was allowed in the interest of the trainers [or drillmasters]. Top athletes with national importance, who get made famous by the D.K.H [? Something like Deutsche K. Heer = German K. Army], should find the support in the interest of the German Sport. They, therefore, as far is their duty allows it, enabled to train and take part in tournaments regarding their special fields after the recruitment training. The same support should be given to the hand and football team of the Liga-class. 

The sportsmanship and sporting activity of a unit can be supported, if the sport enthusiastic soldiers maintain the reputation of their unit part, on basis of their abilities, and can be enhanced further through gathering them in the Army Sports Association. The Army Sports Association will mainly support the hand and football players of a battalion etc.[?] and combine them to one team. For the participation of those teams in tournaments [part of the word on the next page] 
 
 
**- 21 -**
 
tournaments and series games of the German Reichsbund for Body Exercise its necessary that they are part of it.  
Only an active leader may lead the Army Sports Association to flourish. 
In each presentation in the public, the Army Sports Association has to enhance its receptiveness and reputation through military and sporty behavior of its members. 

The voluntary sport counts as military service if it was permitted in the interest of the training by the responsible discipline advisor. If the soldier would come to harm, it would be regarded as service injury according to § 4 of the Army welfare and provision [?] law.

---

# 22-25
#### Needs redone

---

**- 22 -**

IV. The exercises Introduction remark.
 
a) The from every soldier required mandatory exercises. b) Exercises, which are more advanced and especially valuable for the officers, sergeants and gifted soldiers. These exercises must be included in the separate training fields, in form of small print, without description. c) In special cases a battalion etc. commander may skip training parts if there is a shortage of training utilities (Missing utilities, swimming possibilities etc.). On the other side commanders may determine exercises as required if they are necessary for the subject of his troop. 

A. Physical school 

19. The physical basic training takes place in the body school. It includes movement games, gymnastics and exercises at the gymnastic apparatuses. 

**- 23 –**

20. Movement games. 
21. 
Simple games like running, dragging, throwing, ball and party [politics] games are the easiest way, to make untrained soldiers agile and flexible. In the process especially the joy of activity gets reinforced. The frolic [?] in game is therefore part of the start of the training. Furthermore the games server as balance and relaxation after hard service, therefore they have to be continued over the hole year. 

Example for games: 

a) Steeplechase run game (only content [?]). Behind a drawing line multiple lines of up to 10 standing players, 25 m in front of them are 4 players with 5 steps between each other, which are the obstacles. The first one in a kneeling, the second in straddle[?] position, the third one in a squatting[?] position, the fourth in resting position. On “Go!” the first player of each row runs towards the obstacles; the first one must be jumped over, the second should be crawled under, the third must be jumped over and the fourth must be circulated as turning point. After that the player return to his row, where the next player starts, after a check with the other, at the drawn line. The game ends after each player of the row completed all obstacles. 

b) Fight against the row. About 12 players stand in one row, everybody grabs the hip of the man in front of him, who has his extended arms. Opposite of him stands 

**- 24 –**

A single player as attacker, whose task it is to split the clinger player at any spot. The first player is only allowed to use his extended arms to block the attacker but without holding on to him. The las row players swing suitable if the attacker attacks from the left, to the right or the opposite. If the attacker manages to split the row at some point, the attacker steps at the end of the row and the first one of it replaces him as attacker. 

c) Tug of war. 

d) Medicine ball relay. The relays (up to 20 members) stand in one row with about 3 m distance exact as man in front with wide splayed legs, the first and last player at a mat. On “Go!” the ball will be rolled through the legs of each relay. Every player tries to accelerate the ball with his open hand. The last player receives the ball kneeling and walks on the right side of the relay on the place of the first one. In that time each player slips on the place of the man behind him. If the ball slips out of the relay left or right before he arrives at the end they must continue, with the ball at its current position. Winner is the relay, which is first with its players and ball again at the original position. This game can be played alternating with ball through the legs or over the head. 

The relay gains speed if the game is played with 2 or 3 balls. The balls will [part of the word at the next page] 

**- 25 –**

will be passed on “Go!” with about a distance of 8 m.  But every relay member only brings 1 ball to the front to continue with the game, only the last player has to pick all balls and bring them back to the tart. The relay with its balls at the start point wins. Further training possibilities are optional. 

e) Ball over the cord [?]. Inside of an marking square of an length of 10 – 30 m und width of 6 – 12 , which is split into 2 equally big fields through a 2 meter high cord, two teams of about 5 – 6 man arrange. The players of each team split themselves into front and back players. The ball is thrown over the cord and must be caught. The ball must be thrown from the point where it was caught or stopped or where it crossed the field markings. Each time the ball touches the ground the team on the other side gets a point. Points can be achieved when: 

1. the ball touches the cord or is played under it, 
2. the ball touches the ground outside of the field (the team of that side gains 1 point). Throws, which get touched before they hit the ground count as though it was thrown by the team of the receiver. The markings belong the field. 
3. an enemy crosses over the cord while throwing.

Winner is the team which first achieves 20 points, after 10 points playing sides are switched.

---

# 26-28
#### Rough

---

**- 26 -**

f) Impact ball relay with the medicine ball. The games of 8-10 men are played in series as a relay race. No. 1 as a player faces the ball of his relay with about 2 m distance between him and player no. 2. On the command "Go!" the player throws the ball to No. 2, who then throws the ball back and sits down. Then No.3 gets the ball thrown to them by player no 1. and then throws it back as well and sits down, etc. Then the last player of the relay team, once he has received the ball, runs to the place of player no 1. who takes the lead in his turn. All participants stood up during the run-up and took the place of the man behind them moving two meters towards the goal distance. last player becomes first, the first becomes second and so  the relay series starts anew for each team with each player. The winner is the relay team whom arrive first with the ball at the goal line by relaying the ball in a series to get there.

(g) Equestrian squadron. Behind a painted line, the players stand in line (each row up to 10 men strong). In 20-25 m dispersing from the painting line, a target is marked in front of each row. On the command "Go!" the second of each row jumps on the back of the first and is carried from this to the target. Here, No.2 jumps off, runs back to the row and runs No. 3 to the finish line. now no. 2 and No.3 runs back and picks up No.4 and so on until all the players are at the finish line.

h) Ball under the cord. In a demarcated rectangle of 16 m length and 12 m width, which is divided by an 80 cm high leash into two equally sized fields, 2 teams of 6 players each play. The game is played with 3 Medicine balls, which are rolled under the leash. Running in your own field is allowed. Any ball that touches the rear of the playing field boundary by the opposite team is considered a point and can be achieved by all sorts of techniques.

**- 27 -**

is considered a point. The a ball out of bounds on the sides of the field is not evaluated. A point is also scored: 1. if a ball or player touches the leash, 2. when a ball is thrown over the leash. The winner is the party that scores the highest in 10 minutes. 1 arbitrator is required for each game.

(i) People's Ball. In a playing field of approximately 20 m in length and 10 m width, divided into two equally sized fields by a center line, two equally strong teams, each up to 20 players, face each other. With a hand ball, both parties try to throw the ball at the other team. At the start of the game, one player from the two parties is behind the opposing playing field. He participates in the throwing or tries to play the ball to his team in such a way that it is possible to hit an opponent with a well placed throw. Every person hit must leave the field. All discarded people surround the opponent's game limits and continue to participate in the game by playing the ball to their team or dropping the ball away from the opposing teams advantage. The winner is the party that first defeats the other teams ranks until no one is left on the other team. At the start of the game, the ball is thrown up by the referee on the middle line. The party that catches the ball immediately starts the game. The game becomes more difficult when playing with 2 balls, or when it is allowed to catch the ball or to fend off the ball by smacking it away by hand.
j) A handball, rugby or medicine ball can be played on a field of any size with any number of players. Gravel or hard frozen ground is not suitable as a playing surface. In order to avoid injury.

**- 28 -**

However, it is recommended to play in a field of only 30 m in length and 15 m wide with 6 players on each side. The goals are represented by two medicine balls 4 m apart. They are 3 m in front of the lower boundary lines of the playing field.

Playing: The 6 players, divided into 3 strikers, 1 runner and 2 defenders, try to place the ball on the opponent's goal line. At the start of the game and after each goal, each team is on their goal line. The ball is thrown into the middle of the field at the whistle. The ball may be thrown and pushed with the hands or carried as much as any steps. The ball can be taken out of the opponent's hand. The owner of the ball may be held below the arms by the opponent as long as he holds the ball. The opponent held in this way must play the ball within 5 seconds. It can be played behind the goal, but a goal can only be set from the front. The lines must not be passed through from behind.
A free-throw is imposed: a) in case of intentional pushing or kicking the ball with the foot, b) if the opponent is held incorrectly, c) if the player is not playing the ball within 5 seconds of being held, d) in rough and dangerous play.
If the ball exceeds the playing field boundaries, the ball is brought back into the field by throwing it in.

If two players make an error at the same time, a referee's throw is given.

---

# 29-35
#### Rough

---

**- 29 -**

The duration of the game can be set as desired. It is recommended to play about 2 by 10 minutes.

1) Examples of other games: flying ball, hunter ball, neckball, tug of war games over a line or from a circle as a competition, etc.

21.Gymnastics. 

The gymnastics practitioner should log, and maintain ability by stretching and strengthening the muscles as planned which thereby prepare the body for athletic performances. It consists of 1. gymnastics without equipment, 
2. gymnastics with apparatus or equipment.
22. Gymnastics without equipment.
The starting positions for the exercises under a-c find:
 resting position (Fig. 1), 
 legs open or square position (Fig. 2), 
 standing straight position (Fig. 3), 
 step position (Fig. 4), 
 sitting position (Fig. 5), 
 knee position (Fig. 6),

**- 30 -**

Push up position. (Fig. 7), abdominal position (Fig. 8), laying position (Fig. 9).

**- 31 -**

Directory.
On the command "To the gymnastics to the left (r.) opened march (march, march)", the department opens so that the front line from the right (l.) Wingspan from man to man is 3 steps between each one and ine line to the rear of each man is to have a 2 step distance. step on the gap of the man in front as they step forward in unison. On the gymnasium floor, every man takes the resting position. On the command "Right (l.) completed - March! (March, March)" the first line-up is taken again and marched on the shortest way.

Practice of gymnastics.

The exercises are to be done in the alternation of arm, torso and leg exercises. The exercise group begins with relaxation exercises, followed by stretching and strengthening exercises. After several strenuous exercises, a relaxation exercise follows appropriately. In time, the gymnastics without equipment should not last longer than 15 minutes. The exercises are announced and presented. If the exercise is known, the presentation and lesson can be omitted. Each exercise starts and ends on the command "Start" or "Stop" in the rest position. During exercises in the sitting position, kneeling position, push-up to lift yourself, as well as in the abdominal and laying position, the resting position is taken after the command "stand up". Occasionally, suitable exercises can also be carried out uniformly after counting or on the commander's command.

-32-
a) Easing exercises

1. Standing straight, jump (Fig. 10): When bouncing, arms and shoulders must be kept loose.
2. while in the air, swing your arms upwards and spread your legs into a square position(Figure 11):

The arms are loosely swung into the air slanted high and then converged above the chest or head and then back through towards the sides of the body . The same exercise can be carried out with knee-swinging or jumping-jacks as well as with high and low swings of the arms.

**- 33 -**

3. Step position, leg swinging (Fig. 12):
The deferred leg is swung back and forward, where the knee and ankle of the swinging leg must be loose.
4. Square position, mill circles (Fig. 13):
With holding up one arm, both arms circle one after the other closely to the body first forward and then on command backwards. The torso must not be turned when the arms are circled.
Sports regulations for the army. 3

**- 34 -**

5. Square position, hip circles (Fig. 14):
The circling of the torso takes place with pressed knees alternating left and right in alternating left and right. The arms swing through the upholds or hang loosely down.
6. Square position, circles of both arms (Figure 15):
From the arms held high, both arms are then circled forward first and then backwards. Same exercise with knee-jerk or jumping-jacks.
7. Step position, arc swinging of a leg (Fig. 16):
The deferred leg is swung forward and then sideways in the arc back to the starting position. The leg rises and then lowers back into the toe.

**- 35 -**

8. Square your hips and bend forwards (Abb. 17): 3*

---

# 36-38
#### Good

---

**- 36 -**
From the upright position, both arms swing forwards through the invert position, backwards-upwards, with simultaneous forward bending, with squats and stretches.

9. Side straddle, rump turning (fig. 18):
With both arms held to the side, the torso is alternately swung to the left and right, with the respective rear arm remaining stretched, while the front arm is loosely swung to the chest. The legs are pushed through.

                            Picture
                            
b.) Stretching.

1. Side straddle, Abdominal crunches forward (fig. 19):

With straight legs, the trunk is bent forward, far enough for both hands to touch the ground. 

**- 37 -**

The same exercise can be performed with one or two pushes or with folded hands
swinging from the upright position through the legs.

2. Side straddle, Abdominal crunches backward (fig. 20):

                            Picture
                            
By slightly sliding the knees forward and pushing up the hips, the trunk is bent backwards. The feet have to stay on the ground with the whole sole, the arms hanging down casually.

3. Side straddle, Abdominal crunches sideways (fig. 21):

With both arms held up, the trunk is alternately swung to the left and right.

**- 38 -**

4. Side straddle, falling into deep squat (fig. 22):

                        Picture
                        
Both arms swinging, the knees bend quickly, with the body resting on the whole sole. The trunk is curved, the head retracted.

                    Picture
                    
5. Running position, spread legs sideways (fig. 23):

One leg is swung sideways with the standing leg firmly planted to the ground. Alternate between both legs.

---

# MISSING PAGES 39-44

---

# 45-48
### Ok, missing some items

---

**- 45 -**

[MISSING BULLET C AND FIGURE 37]
1 Pushups Headfirst, Squatting and Stretching of the Legs (fig. 38):
[insert fig. 38]
The legs are in turn stretched out and reverted into a squatting position. This exercise can also be performed with one single leg at a time, and with a singular push to create more momentum.

**- 46 -**

2 Supine Position, Circling your Legs (fig. 39):

You lift your legs up from the supine position with lateral support (as seen in fig. 39) and continue circling them around without them touching the ground.

[insert fig. 39]

The circling can be executed with closed legs as well as with continually straddling, sinking, or closing your legs.

[insert fig. 40]

**- 47 -**

3 Supine Position, quickly lifting Torso and Legs (fig. 40):

Torso and legs are evenly lifted up so that the hands touch the feet.
4 Supine Position, quickly lifting and lowering the Torso (fig. 41):
Lifting the torso is initially performed with a bent body and later with a straightened one. A helper holds down the practitioner's feet while lifting with a straightened torso.

[insert fig. 41]

5 Pushups Headfirst, Elbow stretching and curling (fig. 42):

[insert fig. 42]

The curling of the elbows is performed slowly, the stretching quickly.

**- 48 -**

6 One-Legged Squats as Partner Exercise (fig. 43):

Two practitioners, grabbing each other's hands, simultaneously perform a deep squat, one with the left leg and one with the right. When bending the left leg, the right one is pushed forward.

[insert fig. 43]

7 Seat with Legs stretched out and circling of the legs as Partner Exercise (fig. 44)

[insert fig. 44]

The two partners sit opposite to each other, one has his legs closed and the other has his opened. The closed legs occupy a higher position. Successively, the practitioners perform various leg exercises: Lifting, straddling, lowering, and closing of the legs. While closing the legs, the ground should not be touched. [LINE BREAKS OVER PAGE]

---

# MISSING PAGES 49-52

---

# 53-58
#### Good

---

**- 53 -**

[BROKEN OVER PAGE] Safety measures: The leader has to ensure sufficient distances and intermediate spaces for the practitioners. the practitioner must have a workout leader. 
a) Exercises with the medicine ball: this requires two practitioners standing opposite of each other. the medicine ball is caught in the exercises by one partner catching the ball at his chest while reaching to catch the ball thrown by the other practitioner. 

[INSERT FIGURE 53]

1. Step one position, throw the ball as a high throw 
(Fig. 53): when throwing, the back is slightly bent back and the ball is swung into a high overhead position and thrown forward in a high arc towards to co-practitioner. 

**- 54 -**

2. Step position, throw forward as deep throw 
(Fig. 54): From the high hold, the ball is thrown forward by swinging forward and applying resistance from the torso to hip height towards the co-practitioner. 

[INSERT FIGURE 54]

3. Step length position, throwing the ball backwards over the head 
(Fig. 55): The ball is swung forward under the hips and then thrown backwards over the head by stretching and planting the bodies weight into the toes. 
4. Side-digging position, throwing backwards under the hips 
(Fig. 56): From the high hold, the ball is thrown forward and downward through the spread legs under the hips as a fast underhand throw to behind the practitioner. 

**- 55 -**

[INSERT FIGURE 55]

**- 56 -**

[INSERT FIGURE 56]

5. Step position, straight thrust right and left 

(Fig. 57): Throw by recoiling one arm and extending the other arm forward in a throw, the ball is pushed at shoulder height from the practitioner to the copractitioner. The 

[INSERT FIGURE 57]

**- 57 -**

Effect can be increased by the increasing speed and force of the throws and impacts. The same exercise must also be carried out as a long-range throw whereby the starting position is taken for an aimed throw envolving a forward step with the ball in a recoiled position prior to slinging the ball forward in a long distance throw. 

6. Step position, sling back the ball right and left 

(Fig. 58)

7. Exercises 1, 2 and 5 can also be exercised while sitting. 

[INSERT FIGURE 58, 59, 60]

b) Exercises with the kettle bell: Using this instrument can be used in a gym and also must be used outside a gym to complete all excersizes in this section. 

1. Pre-swinging (Fig. 59): 

The kettle bell is first pre-raised with outstretched arms. It is then swung through the open legs under the torso with the body absorbing the swings intertia into a squatting position. The same exercise with one arm alternately left and right is also effective. (Fig. 60). 

**- 58 -**

2. Side swinging (Fig. 61): The kettle bell is swung in front of the body with one arm alternately switched to the right and left in the swing from below the torso to the side. 

[INSERT FIGURE 61, 62]

3. Circles with one arm (Fig. 62): After previous swinging sideways, the kettle bell is circled alternately left and right around to the front of the body. 

---

# 59-66
#### Ok

---

**- 59 -**

4. Circles with both arms (Fig. 63):

After swinging sideways, the kettlebell will be swung alternately left and right around the head. Over the head the arms are bent lightly, in front of the body they are straight.

Fig. 63 Placeholder        Fig. 64 Placerholfer

5. Kettlebell clean (Fig. 64):

The Kettlebell will be thrown through the squatted legs first. At the end of the following upwards movement the kettlebell will be let go and the it will be catched with bend arms and knees over the shoulder with an open hand.

6. Kettlebell flip

After the previous swinging with one arm through the squatted legs the kettlebell will be let go in the upward movement and catched after id did a full turn.

**- 60 -**

[LINE MISSING AT THE TOP ABOVE PICTURE]

Fig. 65 Placeholder    Fig. 66 Placeholder

e) Exercises with the throwing hammer

These exercises are usually meant to be used in competitions. Doing this it is sharply advised to have a free range, and to leave enough space between the practitioners.
Starting position: The practitioner stands in a form of Squat with bend knees in front of the at the ground lying weight so that he can grab the weight with an overhand grip and straight arms.

Swinging throw forward through squatted legs (Fig. 67)

Swinging throe backwards over the head (Fig. 68)

Circling of the weight (Fig. 69)

**- 61 -**

Fig. 67 Placeholder                

Fig. 68 Placeholder

Fig. 69 Placeholder

**- 62 -**

Out of the starting position the weight will be swung backwards through the opened legs, then forward again, swung from the right side and circled left over the head.

Fig. 70 Placeholder

 The weight will be swung deep behind the back on the right and high in the front left, in front of the body the arms are in a straight position, below the head they are bend.
 
 Throw out of the standing position
 
After circling the weight once or twice, the weight is to be thrown backwards with straight arms over the left shoulder (Fig. 70.)

**- 63 -**

d) Exercises with the barbell (Fig. 71 and 72)

While exercising there should some kind assistance to combat the barbell falling backwards.

Clean with one arm and both arms

Jerk with one arm and both arms

Fig. 71 Placeholder       

Fig. 72 Placeholder

e) exercises with the ball (5 and 7 ¼ kg)

Throwing the ball straight up left and right, catching with the same or the other hand. Throwing from the right side over the left shoulder and catching it with the left hand or vice versa.

24. Exercises on gymnastic equipment

The gymnastic equipment is to be used to strengthen the body and for obstacle gymnastics.

Explicit Gymnastic posture is not required.

**- 64 -**

Safety precautions:

To prevent accidents help is strictly advised. Help is foremost necessary when doing Jumps over the gymnastic equipment, as well as dismounting the from the horizontal bar and parallel bar. In the beginning help comes from the teacher, later from trained personal. The dismounting from the equipment is to be cushioned through a mat or deep raked sand pit.

Fig. 73 Placeholder

a) Reck

1. Pullups with a bar at reachable height. It is advised to train with underhand grip and reverse grip, the chin has to get above the pullup bar
2. Swinging out of a standing position             -------      the bar has to be on the 
3. Turn left and right (Fig. 73)                            ---^    height of the head
4. Squat
5. Muscle up (Fig. 74)
  b) Parallel bars
1. Dips (Fig. 75)

**- 65 -**

Fig. 74 Placeholder

Fig. 75 Placeholder

**- 66 -**

2. Climb over (Fig. 76)
3. Sideway jump over both parallel bars from a run-up (Fig. 77)

Fig. 76 Placehoder

c) Parallel bars with different height:

Jumping onto the lower bar with feet first, or just sideway jump over the higher bar (Fig. 78)

Fig. 77 Placeholder

Climbing over the low bar first, then the higher bar, then vice versa (Fig. 79a, b, c).

---

# MISSING PAGES 67-69

---

# 70-72
#### Ok

---

**- 70 -**

d) Horse, cross without lumps.

1. land your hand on the edge left or right (Fig. 82).
2. Squat with both feet and jump forward (Fig. 83).

**- 71 -**

Fig 84

Fig 85

3. Tuck your heels to your rear (Fig. 84).
4. Running and jumping up and jumping forward (Fig. 85).

**- 72 -**

5. Free jump over the horse's height of 1.10 m (Fig. 86).
  (e) Horse or long box:
1. Squat with one foot landing on the back of the horse and run over the device with a low jump forward and skip any presented obstacle (Fig. 87).

Fig 86

Fig 87

---

# 73-83 
#### Good

---

**- 73 -**

2. Drop down off of horse in front from a squating position.
3. Giant grätsche-

(f) Strong perch:
  
Climbing and hanging are particularly powerful exercises. When climbing, care must be taken to ensure that the climbing is correct. Hangeing can also be practiced by those who are proficient enough.

(g) Obstacle squadrons:

In the case of obstacle relays, the gymnastics equipment is regarded as obstacles. By appropriate assembly of equipment, etc., obstacle courses are set up, which are overcome as quickly as possible in the competition. They are skipped, overtaken, undertaken and exceeded. In particularly difficult exercises, helpful-orders must be given.

Relay examples of the simplest type:

1. Climbing and crossing a waist-high bar a horse.
2. Jumping over the horse, squatting on or standing on the horse, running over the back of the horse and jumping over an presented obstacle such as jumping a hurdle or bar or a higher horse.
3. Climbing and jumping one bar to another of unequal height, squatting on one horse and jumping over to another while overcoming obstacles.
4. Cross over a horse, turn around and backflip. Turn away.
5. Crawl under a bench or other obstacles, free jump over a perpendicularly crossed horse and climb or hang onto a rope.

**- 74 -**

25. Floor gymnastics.

The ground exercises make the body supple and flexible. They should only be done on soft surfaces. Difficult ground exercises, such as forward somersalts and flips (saltos), may only be carried out by people who have the necessary physical preparation and suitability.

1. Forward Roll (Fig. 88): Exercise possibilities:

(a) rolling forward from the approach and start-up;
b) Forward roll several times in a row;
c) jumping and then rolling forward into the handstand,
d) Handstand and then handstand with subsequent roll forward.

2. Protect yourself.
3. Roll backwards (Fig. 89 a, b, c). Exercise possibilities:

a) Roll backwards from the seated position to the kneel position.
b) Roll backwards from the squatted kneel position to the handstand.
c) Roll backwards from the handstand to a standing position.
d) Roll backwards with closed and squated legs.
e) Roll backwards into the volatile handstand.

**- 75 -**

Fig 89

**- 76 -**

4. Pike roll (Fig. 90):

An assistant kneeling next to the practitioner can facilitate a forward pike roll by applying slight pressure on the back of the head "the head-on-the-breast part of the pike roll" of the practitioner without exaggerating it so far that the practitioner falls on their back.

Exercise possibilities:

a) Pike roll from excersise equipment (jumping with both feet).
b) Pike roll from a running start (jump with both feet).
c) Pike roll over 1 to 3 adjacent people (laying flat).
d) Pike roll over 1 to 3 side by side people (benches).

5. Handstand.

The assistant helps the practitioner.

6. Roll forward, first learn a handstand with the bench, then finally without a bench.

7. Roll sideways (cart-wheel).

8. Roll forward freely (Salto).

**- 77 -**

27. Exercises on the rungs wall.

The exercises on the rungs wall are mainly stretching exercises.

1. Stand forward on the fourth rung, handle at hip height, bend your torso with one or two recoils.
2. Stand forward on the fourth rung with open legs, handle at hip height, falling into a deep squat with recoils.
3. Hang forward or backward at the second-highest rung, swinging the legs from the hip alternately left and right.
4. Seated back with open legs close to the rung of the wall, handle at ample height, lifting out of the seated squat into a legs bent outward position from the hip. try the same exercise with closed legs while lifting your toes as high above your waist as you can.
5. Stand next to the fourth rung, handles at shoulder height, legs spread to the side, lift and lower yourself.
6. Stand backwards on the fourth rung, handle at hip height, lower then raise yourself with legs spread.
7. Hang backwards at the second-highest rung, lifting and lowering your legs.
8. Stand forward with hips bending legs outward, reflex your legs forward and down.
9. Stand and flex your knees and torso to the side, stretching horizontally and recoil back into place.
10. Stand close to the rungs on the wall, bend your hip forward and detect the second rung. using your arm bend and stretch your knee with pressure.
11. Reclining up and down with your hand on the handle of the third rung, mutualy bend and stretch your arm.
12. Stretch excersises can be done with partners.

**- 78 -**

B. Athletics

28. Through the exercises of athletics, the soldier learns to perform the natural movements of running, jumping and throwing in a practical and energy-saving manner, thereby increasing his performance in all areas of military service.
The aim is to provide a wide range of training, if possible, with the addition of voluntary exercises. Through competitions and performance measurements in the practice hours, the ambition of the soldier is to be promoted in a healthy way and his voluntary commitment is achieved until the last forces are deployed.
29. The run.

a) General: The run is the most valuable exercise. Through it, stamina and speed are acquired and the internal organs (heart, lungs, blood circulation) are particularly stimulated and developed. When running, a loose and buoyant movement is to be sought and any unnecessary tension or cramping of the muscles must be avoided. Particular attention must be paid to a full swinging of the legs forward (Fig. 91). The faster the run, the more the upper body pushes forward, the stronger the legs are stretched during repulsion and the stronger the arms swing (Fig. 92). With slow running, the torso is upright, arms and legs swing lightly and loosely. The feet point in the direction of travel. At the fastest run, the foot springs up, with medium-fast and slow running the foot is placed with the whole sole forward

**- 79 -**

and rolled off. Breathing takes place through the mouth and nose. Thorough exhalation must be ensured.

b) Teaching: The correct running style is acquired through the following running exercises:

1. Unforced running at a slow pace on the track (each level course is suitable).
2. Run with short, loose steps dribbling a ball with the feet (football/soccer run) to achieve the required looseness of the leg and arm work.
3. Increase runs over 100 to 400 meters to train in each running style. The speed of the run is gradually increased during training.
4. Runs with tempo change. The runner reccurs speed repeatedly, initially from medium-speed, then to a slow speed then to a run with the highest force and then passes back into the initial pace.

**- 80 -**

c) Off-road running is the best training of endurance. The soldier learns to adapt to the difficulties of the terrain in his running style. The training of endurance depends on the length of the track and the pace. Shorter runs (2-3 km) at a lively pace train endurance as well as longer runs (up to 10 km) at a slow pace. The training begins with slow runs in light terrain (solid ground) over 2-3 km, which are interrupted in the beginning with breaks between stages in training. The length of the track, the speed of the run and the difficulties of the terrain are gradually increasing.

d) The quick rise. The fast-track is carried out over the distances of 100, 200 and 400 m (short distance run). Exhausting excersises, boost runs, tempo changes are necessary pre-exercises to achieve a performance in the short distance run. Repeated short runs over 60 to 100 m, occasionally over 200 to 300 m, for the particularly heavy and important 400 m track also over 500 to 600 m, give the necessary endurance to be able to pass this route at a sharp pace. Every short-distance runner should also be trained in the start and relay change. The usual relays go over 4 by 100 m and 4 by 400 m.

e) Set up to start running by placing dominant leg behind the foot that is placed behind the starting line bending the knee down on the other leg so that the knee of the dominant leg is located near the ankle of the forward foot, the tip of the foot is tightened into the starting holes and are carefully and deeply seated into back walls fixed in place ready to propell forward.

**- 81 -**

On the command "On the squares" the runner puts his feet in the starting holes, kneels down and puts his hands on the starting line, so that the fingers touch the ground with their fingers pointed inwards and the arms stand apart shoulder-wide (Fig. 93).

On the command "Done" the runner lifts the knee of the dominant posterior leg so far from the ground that the leg in the knee forms an angle of more than 90°, pushes the upper body forward, transfers the body weight to the stretched arms and breathes deeply. The view is slanted forward to the ground (Fig. 94). On the run-off command "Go" (shot) the runner pushes himself with both feet out of the starting holes. The first steps are short, fast and loose, the arms swing sharply angled as the legs. Running is gradually becoming a natural
Sports regulation for the army. 6

**- 82 -**

running posture. After the start, 20 to 30 m must always be sprinted through.

Teaching:

1. Practice the individual positions.
2. Easy starts, note that the arm work is correct from the first step.
3. Start over 20 to 30 m at full speed.
4. Pre-set starts and runs (worse runners get a target) up to 60 m. They are often used as an incentive for unintended runners.

f) The change of staff during the relay race. The change of staff is the technical prerequisite for the team effort in short distance running.
The runner receiving the baton stands at the beginning of the 20 m transfer spot and awaits the runner approaching at the fastest pace with the rod in his left hand. As soon as he has reached a mark designated 6-8 m before the beginning of the transition location (the distance depends on the speed of both runners), the receiver goes off in the sharpest run with full arm work. Shortly before the bringer has given it, he stretches out the right arm to the back (hand spread out stretching from the body), the bringer simultaneously pushes the rod from under into the hand of the receiver with the front extended left arm (Fig. 95). The rod catcher immediately takes the rod into his left hand.

**- 83 -**

Teaching:

1. Practice the relay change while walking, then slow and fast running.
2. Relay change in the 20 m location with auxiliary stamp determination. In order to determine the auxiliary marks safely, it is necessary to pass through the full distance in the competition order of the stops.
3. Practice competitions in different seasons. 
30. Other running distances are: medium distances over 800 m, 1000 m, 1500 m. Long-distance runs over 3000 m, 5000 m, 10 000 m, 25 km, 42.2 km (Marathon). Hurdles over 100 m, 200 m, 400 m. steeplechase (obstacle course run) over 3000 m.

---

# MISSING PAGE 84

---

# 85-90
#### Ok

---

The tuck jump is started by running forward, both legs tucked to the body while jumping and ends by landing on both feet (fig. 97).
 
                    Picture
                   
The Scottish jump is performed by running from the sideways, so that the swinging leg is facing the bar. The legs are swung over one after another; the jumper lands on the swinging leg (fig. 98).
 
                    Picture
               
The run-up to the high jump is a moderate run in which the last three to four steps are performed powerfully.
 
c) The long jump.
    The long jump is a tuck jump, in which the lower legs are swung forward for landing. To avoid falling back,
    the jumper swings his arms forward and pushes his knees forward when he lands (fig. 99, fig. 100).
    The run-up to the long jump is an uphill run in which the maximum speed must be reached several steps before jumping off.
    Speed ​​of the run-up and height of the jump make the distance.
   
                    Picture
               
d) Teaching style of jumping:
    1. Jumps from the standing position to strengthen the bounce and to learn the necessary stretch when jumping.
        a) Long jump from the standing position.
        b) Triple jumps from different standing positions (on both legs, on one leg, stride leap, sporty triple jump, hop, step, jump).
        c) High jump from the standing position as tuck jump.
       
    2. Jumps with a light run-up.
        a) Jump with both legs, high- and long jumps.
        b) Jump with the takeoff leg and relaxed but high long jumps.
       
    3. Long- and wide jumps with run-up.
    4. In cross-country runs, jumps over ditches, hedges, in sand pits, etc. are carried out.
   
Predisposed jumpers maintain their jump style in the high- and long jump, if appropriate.
 
32. Other jumping exercises are:

    Long jump as running jump (including correct run-up technique).
    
    High jumps in various techniques (including roll-overs).
    
    Pole vault.
    
    Triple jump with run-up.
   
33. The throw.
    It's prohibited to throw back the devices use by exercising person. The throwing field is to be kept free at the front and on the sides by exercisers.
    
        a) The hand grenade throw. The hand grenade throw is practiced as a long- and goal throw.
   
    Throw from the standing position: The hand grenade is held firmly but not convulsively in the throwing hand. The body weight rests on the right leg, the upper body is slightly bent forward.
    
    The throwing arm is almost stretched back (fig. 101).
   
    When throwing, the left leg is lifted first with the body leaning far back from the throwing direction.
    
    With the left leg returning to the ground, the right one is stretched, the right hip and shoulder thrown forward and the throwing arm abruptly snatched forward past the right side of the head (fig. 102).
   
                    Picture
                   
    Throw with run-up: The run-up is about 15-20m. It is an accelerating run, in which the last 2 to 4 steps are carried out so that the right leg is placed over the left (cross step) and the thrower thus enters the throwing position.
    
    At the run-up, the hand grenade is carried in front of the body. In order to avoid shoulder and elbow injuries, only light throws are required in the beginning, and performance throws later.
   
    Teaching style of hand grenade throws:
   
        1. Preparing, light throws from the standing position to learn how arm and body work in conjecture.
        2. Throw from slow run-up, practicing the cross step.
        3. Throw from moderate run-up, practicing the cross step.
        4. Throw from fast run-up, defining a mark at the beginning of the cross step.
        5. Hand grenades target throws.
       
B) The shot put. The thrower stands with his left side to the throwing direction. The ball lies on the finger roots, the thumb is spread out.
   
                    Picture
                    
The right leg is bent, the upper body slightly bent forward, the ball lies on the right shoulder (fig.103).

When thrown, the left leg raises and lowers a little to the left of the throwing direction on the front edge of the circled floor.

The push is done by strongly extending the right leg by advancing the whole right throwing side over the stretched, stiff left leg (fig.104) and by straight pushing of the throwing arm at an angle of 45 ° over the edge of the circled floor (fig. 105).

The right leg must not lose contact with the ground until the ball has released the hand.
 
                    Picture
                   
Teaching style:

    1. Brief practice of the starting position and the process of movement without a device.
    2. Push from the standing position with a 5kg ball. Practicing of partial movements is to be avoided.
    3. Push from the standing position with a 7,25kg ball.
    C) The stone toss. The stone toss requires the same technique as the shot put when standing upright (fig. 106, fig. 107). When throwing, the stone is carried one-handed in front of the chest (fig. 108).

---

# 91-95
#### Needs Review

---

**- 91 -**

the stone is carried with one hand in front of the chest (Fig. 108). By moving the right leg

Fig 106, 107, 108

**- 92 -**

Over the left (cross-step), the thrower moves into the discharge position.
Teaching Method:
1.    Push off the stall right and left
2.    2. Running with the stone and learning the cross-step.
3.    Push with a full start of 15-20m length right and left.
34. Other throwing exercises are:

Shot-put with pitch,
Discus throw,

Javelin,

Hammer throw.

Swimming

35. Supervision and Classification. During swimming lessons, an officer or a non-commissioned officer trained as a swimming instructor is to be designated as the inspector. He is responsible and arranges the necessary security measures.
36. Safety Measures: The supervisor and teaching staff must observe the following rules:

a) Ear, eye and skin patients should not go into the water without medical permission;
b) People who have been ill must be specially watched;

**- 93 -**

c) People should not go into the water with a full stomach or while overheated;

d) After great efforts (marching) or little sleep (watch), efforts to swim must be avoided.

e) Do not use outdoor baths when the water temperature is low for too long. For continuous swimming, the skin must be greased to reduce the heat loss.

The site elders issue safety regulations according to legal conditions. In accordance with the general safety regulations, the inspector must arrange the division of troops, the distribution to the free-swimming and non-swimmer pool, the change of departments, the classification of the teaching staff and the emergency services (the latter, if necessary, in consultation with the lifeguard). The safety regulations are to be formulated accordingly for indoor swimming pools, if the civilian bath master does not lead the supervision.

Special measures are to be taken when swimming outside the institution. Lifeguards who swim next to the students are to be marked by bright caps. A lifeboat (wide wooden barge) must be present. Canoes and folding boats are not suitable as lifeboats. For deep and opaque water, it is advisable to secure students who are already swimming without a leash by corks or bubbles as in diving. 

**- 94 -**

Care should be taken when jumping into water of unknown depth. In each institution are to be displayed:

a)    The safety requirements for the institution concerned;
b)    The panels of D.L.R.G. for resuscitation after drowning.

Not swimming lessons.

37. The goal of the training is the learning of breaststroke and popular backstroke.
Teaching: Lessons are given in shallow water without a device. The equipment-free training has the advantage of the naturalness and the possibility of mass education. It enhances the student's sense of security. First, the students are helped by water habituation with rest and patience about physical and mental inhibitions. Dry swimming exercises have a faster time to teach. The individual exercises should be done first in the countryside and then in the water.
38. Exercises to get used to the water. The pupil must recognize that his body remains almost without movement of the arms and leg on the surface as soon as the lungs are filled with air. Quiet and proper breathing is the first prerequisite. The following exercises serve as a guide:
a) Standing and walking in shoulder deep water, deep inhaling and exhaling.

**- 95 -**

b) Diving:

1. Brief immersion of the head under water, immediate emergence.
2. Inhale, submerge, stay underwater for a few seconds, exhale under water.
3. When diving have open eyes, looking for plates, etc.

c) testing the carrying capacity of the water with deep breathing, first with, then without help, by tightening the legs form ball or package. Stretching of the body in breast and supine position with assistance. Drive in outstretched chest and supine position, first with pulling by a student, then by pushing off the bottom or edge of the pelvis.

39. Swimming movements in the chest position:

a) Leg movement: The legs are squatted with knees wide open, sideways and then closed. 

Practice: In shallow water, the hands support the body on the ground or hold it to the edge of the pool.

c)    Arm movement: From the hold up, the arms are led sideways to forehead height, then the arms and hands are bent under the chest and loosely presented with the palm downwards. 

Practice: In shallow water, the student lies stretched out on the arm of the helper.

c) Breathing: When guiding the arms, it is inhaled quickly through the mouth and exhaled slowly through the nose and mouth during the demonstration.

---

# MISSING PAGES 96-105

---

# 106-110
#### Good

---

**- 106 -**

D - Boxing

50. Boxing serves the military training and education by developing the will and  ability for a fight with the enemy. Boxing promotes mental and physical well-being, self-confidence, toughness, speed and agility. A soldier trained in boxing is equipped with a lot of mental and physical power to perform well in close combat with a weapon.

51. The 1st training level includes all exercises of the boxing school that can be carried out without gloves. In this training level, boxing is an important addition to physical exercises and is to be performed alongside it.

52. The orthodox fighting position is the starting position for attack and defense. The right leg stands about one step backwards to the right of the left leg, with the left leg resting on the full sole, the right resting on the bale with the heel raised, the toe points pointing forward, the knees are loose and little bowed, the weight is evenly distributed on both legs (Fig.124), the upper body is almost upright, the left shoulder is slightly ahead of the right. The left arm as an "attack arm" is set almost at a right angle.The hand is advanced at the level of the left nipple. The right arm is covering the body, just below the chin.Both fists are clenched, the thumb is on the outside of the index and middle fingers (Fig. 125).

53. Footwork: A quick work of the legs is the boxer's indispensable means of either attacking or evading the attacker. 

**- 107 -**

The legs are in continuous movement; crossing your legs or pulling your feet is wrong. A distinction is made between the following movements:

1. Backwards step: the back foot retreats one step backwards, the front one begins to move forward.

   Step forward: vice versa.
   
   Step to the right, sideways: the right foot starts.

                Picture

Step to the left sideways: the left foot starts.

2. Jumping forward, backward, sideways: both legs perform a small flat jump at the same time.

3. The step movements or advanced step movements (three-step), e.g. a left-right-left-step or a right-left-right-step is strung from one leg to the other with a slight shift of weight.

**- 108 -**

4. A good preliminary exercise for footwork is rope jumping.

    a) Jump on both legs with an intermediate jump.
    
    b) Jump alternately left and right with an intermediate jump.
    
    c) Jump on both legs without an intermediate jump.
    
    d) Jump alternately left and right without an intermediate jump.

All jumps are performed flat with a slight bending of the knee joint. As the skill progresses, the last exercise should be carried out especially.

At first, jump only one minute, later up to 3 minutes.

54. The straight punch: The arm ,under strong pressure of the shoulder, connects to the target's head or body. The fist hits with full ankle parts and is turned at the last moment with the back of the hand facing up (Fig. 126).

                Picture

In the right-hand thrust, the left hand returns shielding. If the straight punch can not be applied at a greater distance, the boxer makes a small step with the left foot, in order to obtain a favorable distance to the opponent.

**- 109 -**

Practice: The straight punch is first practiced without and with small steps by counting, then also from the footwork.

55. Defense against the straight punch (partner posing):

    1. A.(Attacker) attacks with a left straight - D.(Defender) takes a step back.
    2. A. attacks with a right straight - D. takes a step back.
    3. A. attacks with a left straight - D. takes a step to the right.
    4. A. attacks with a right straight - D. takes a step to the right.

                Picture

    5. A. attacks with a left straight - D. defends by catching the punch with his right hand (fig. 127),

**- 110 -**

6. A. attacks with a right straight - D. defends by catching the punch with his right hand.
7. A. attacks with a left straight - D. defends by redirecting the punch with his right hand. (fig. 128)

                Picture

8. A. attacks with a left straight to the body - D. defends with the back of his right hand.
9. A. attacks with a right straight to the body - D. defends with the back of his left hand.

The defense is first practiced from a solid boxing position, then from the footwork.

56. The hook: The hook hits the head or body from the side. When striking the hook, the fist is in the same position as when attempting a punch (thumbs up) (Fig. 129).

The arm is more or less bent depending on the distance to the opponent. A wild swing is to be avoided. In the right-hook, the left hand shields the chin.

---

# 111-115
#### Ok

---

**- 111 -**

[LINE AT TOP OF PAGE IS MISSING]

57. Defense against the hook (partner posing): The defense can be done by stepping backwards.

                Picture

    1. A. punches with left hook - D. ducks (fig. 130). Ducking is a low dodge. Head, torso and legs are slightly bent. The blow goes over it.     When setting up, beware of hits. [LINE BREAKS OVER PAGE]

**- 112 -**

2. A. punches with a right hook - D. ducks.
3. A punches with a left hook - D. defends with the back of his right hand, close to his head. (fig. 131).
                
                Picture
    
4. A. punches with right hook - D. defends mwth the back of his left hand.
5. A. attacks with a left punch to the body - D. defends with his right hand or forearm (fig. 132).
6. A. attacks with right punch to the body - D. defends with left hand or forearm.

58. The uppercut comes from below.

At the moment of the hit (trunk, chin) the back of the hand points to the opponent. The arm is bent. The effect of the blow is reinforced by vigorous stretching of the legs. In case of the right hook, the left hand shields.

Practice: The uppercut is practiced like the hook and the straight punch.

**- 113 -**

Picture

59. Defense against the uppercut (Partner posing): The defense can be done by taking a step back.

1. A. punches with left (right) uppercut to the chin - D. defends with open hand (fig. 133).

                Picture

**- 114 -**

2. A. punches with left uppercut to the body - D. defends with the back of his right hand or forearm.
3. A. punches with right uppercut to the body - D. defends with the back of his left hand or forearm.

60. Double strikes: A boxing match will be more versatile if the punches are not just applied individually. Therefore, it is often advisable to apply several punches in quick succession in the form of double strikes. They are practiced out of a standing position and out of the leg work:

    1. Left straight to the body, left straight to the head.
    2. Left hook to the body, left hook to the head.
    3. Left straight to the head, right straight to the head.
    4. Left straight to the body, right straight to the head.
    5. Left hook to the body, right hook to the head.
    6. Left hook to the head, right hook to the head.
    7. Left hook to the head, right uppercut to the body.

61. Shadow boxing (mock battle): The student repeats everything he has learned here, in a casual, self-chosen order, with the teacher paying attention to fluid footwork and clean punching technique.

62. Boxing on boxing equipment: The most important boxing equipment are wall padding, punching balls, sandbags, platform balls, speed balls. Wall padding, sandbags and punching balls can easily be made at home. At the devices hits and punches are first to be practiced as instructed, followed by fighting the device in the way of shadow boxing. [LINE BREAKS OVER PAGE]

**- 115 -**

63. The second level of training includes the combat training. The boxing gloves, weighing 12-14 ounces, are to always be worn during practice. The free fight is only acceptable if attack and defense are mastered.The fight must not transform into a brawl. Therefore, a trainer may not oversee more than 3 to 4 fights at a time, so that he can watch over the fighters and immediately stop faulty or unsportsmanlike fighting. In particular, the instructor must make sure that only equally strong opponents fight with each other. The safety measures initiated by the Boxing department according to competition regulations must be observed.

64. The left-training: First, the awkward left hand is trained in combat.
Partner posing: Follow-Up and Look Back (Stop and Counter) on Left-Handed Attacks: Attack and counterattack occur when the defender avoids or covers a strike by the attacker, either to strike at the same moment or after a short break.

---

# FINAL PARAGRAPH IS MISSING

---

# Missing 116-126

