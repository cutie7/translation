# Sportvorschrift für das Heer
## A translation of the Wehrmacht Fitness Manual

Missing Sections:

- [ ] 10-14
- [ ] 22-25 Needs to be redone
- [ ] 39-44
- [ ] Page 45 Bullet C and Figure 37 Placeholder
- [ ] 49-52
- [ ] Page 60 Line Missing at the top above the picture
- [ ] 67-69
- [ ] 84
- [ ] 96-105
- [ ] Page 111 Line at the top of the page is missing
- [ ] Page 115 Final Paragraph
- [ ] 116-126

### The Plan

#### Phase 1: Collection of Material - Complete

* Rip PNGs of Images
* Transcribe the text into an unaltered German version

#### Phase 2: Elaboration of Material

* Clean up/redraw PNGs
* Translate the German Text
* Design a cover for the book

#### Phase 3: Profit

* Put everything together into the best book /pol/ has ever produced
